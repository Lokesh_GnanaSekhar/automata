package org.framework.automation.Testcases;

import java.util.Map;

import org.testng.annotations.Test;

public class TestTemplate extends TestBase {

	/*
	 * Provide the Data Provider Name and add the parameters respective to the
	 * test data maintained in excel. The test parameter count should match
	 * number of test data columns for the particular test case. It should not
	 * be less or it should not be more. Automatically Iterates to the number of
	 * Test Data rows
	 */

	@Test(dataProvider = "TestDataProvider", description = "This takes 3 Iterations of Test Data")
	public void tc01(String a, String b, String c) {
		System.out.println(a + "----" + b + "----" + c);
	}

	@Test(dataProvider = "TestDataProvider", description = "Different number of Test Data sets")
	public void tc02(String a, String b, String c, String d) {
		System.out.println(a + "----" + b + "----" + c + "----" + d);
	}

	@Test(dataProvider = "TestDataProvider_Optimized", description = "Read Data by passing Column Nmaes")
	public void tc03(Map<String, String> testdata) {
		/**
		 * Display Test case Data Access Data with Column Names This
		 * automatically iterates with available number of testdata rows
		 **/
		String firstcol = testdata.get("First");
		System.out.println(firstcol);

	}

	@Test(description = "Empty Test")
	public void tc04() {
		System.out.println("Test Without testdata");
	}

}
