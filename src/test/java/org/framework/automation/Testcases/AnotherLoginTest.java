package org.framework.automation.Testcases;

import org.testng.annotations.Test;

public class AnotherLoginTest extends TestBase {
	@Test
	public void successLogin() {
		launchApp().clickMyAccount()
		           .login();

	}
	
	@Test
	public void unsuccessfulLogin(){
		launchApp().clickMyAccount()
		           .invalidLogin();
	}

}
