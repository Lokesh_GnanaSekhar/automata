package org.framework.automation.Testcases;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.framework.automation.Modules.DataSource;
import org.framework.automation.Pages.HomePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;

public class TestBase {

	WebDriver driver;

	// @BeforeMethod
	public void beforeTestExecution() {
		createDriver();
		System.out.println("Before Test Execution");
	}

	// @AfterMethod
	public void afterTestExecution() {
		driver.quit();
		System.out.println("After Test Execution");

	}

	public void createDriver() {

		System.setProperty("webdriver.chrome.driver",
				"./src/test/java/resources/chromedriver.exe");
		driver = new ChromeDriver();
		/*
		 * System.setProperty("webdriver.firefox.marionette",
		 * "./src/test/java/resources/geckodriver.exe"); driver = new
		 * FirefoxDriver();
		 * 
		 * System.setProperty ("webdriver.firefox.marionette",
		 * "C:\\selenium\\geckodriver.exe");
		 */
		/*
		 * System.setProperty("webdriver.ie.driver",
		 * "./src/test/java/resources/IEDriverServer.exe"); driver = new
		 * InternetExplorerDriver();
		 */
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

	}

	public HomePage launchApp() {
		driver.get("http://test1.absofttrainings.com/");
		return new HomePage(driver);
	}

	@DataProvider(name = "TestDataProvider")
	public static Object[][] testDataProvider(Method method) {

		Object testData[][] = null;
		try {
			testData = DataSource.getDataForTestCase(method.getName());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return testData;
	}

	@DataProvider(name = "TestDataProvider_Optimized")
	public static Object[][] testDataProvider_Optimized(Method method) {

		Object testData[][] = null;
		try {
			List<Map<String, String>> testDataList = DataSource
					.getDataForTestCase_Optimized(method.getName());
			testData = new Object[testDataList.size()][1];
			for (int i = 0; i < testDataList.size(); i++) {
				testData[i][0] = testDataList.get(i);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		return testData;
	}

}
