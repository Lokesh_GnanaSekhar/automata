package org.framework.automation.Pages;

import org.framework.automation.Foundation.EventHandler;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage extends EventHandler {

	@FindBy(linkText = "My Account")
	WebElement myAccountLink;

	public HomePage(WebDriver driver) {

		this.driver = driver;
		PageFactory.initElements(driver, this);

		if (!(driver.getTitle().contains("Home Page"))) {
			throw new IllegalStateException(
					"Home Pageis expected, but not displayed!");
		}

	}

	public AccountPage clickMyAccount() {

		if (waitForElementToBeVisible(myAccountLink)) {
			click(myAccountLink);
			return new AccountPage(driver);
		}
		return null;

	}

}
