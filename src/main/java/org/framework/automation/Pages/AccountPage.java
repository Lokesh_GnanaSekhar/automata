package org.framework.automation.Pages;

import org.framework.automation.Foundation.EventHandler;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AccountPage extends EventHandler {

	@FindBy(id = "username")
	WebElement userName;

	@FindBy(id = "password")
	WebElement password;

	@FindBy(name = "login")
	WebElement login;

	@FindBy(linkText = "Logout")
	WebElement logout;

	@FindBy(xpath = "//*[@id='post-12']/div/div/ul/li/strong[1]")
	WebElement error;

	public AccountPage(WebDriver driver) {

		this.driver = driver;
		PageFactory.initElements(driver, this);

		if (!(driver.getTitle().contains("My Account"))) {
			throw new IllegalStateException(
					"My AccountPage is expected, but not displayed!");
		}

	}

	public AccountPage login() {

		if (waitForElementToBeVisible(userName)) {
			enterText(userName, "testuser1");
			enterText(password, "testpwd1");
			click(login);

			if (waitForElementToBeVisible(logout)) {
				System.out.println("User has successfully Logged in");
				return this;
			}
		}
		return null;
	}

	public void invalidLogin() {

		if (waitForElementToBeVisible(userName)) {
			enterText(userName, "testuser12");
			enterText(password, "testpwd12");
			click(login);
			if (waitForElementToBeVisible(error)) {
				System.out.println("Unable to Login. Login was unsuccessful");
			}

		}
	}

}
